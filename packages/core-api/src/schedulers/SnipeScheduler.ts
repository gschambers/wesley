import { Injectable } from '@nestjs/common'
import { interval } from 'rxjs'
import { mergeMap } from 'rxjs/operators'
import { SnipeService } from '../services/SnipeService'

@Injectable()
export class SnipeScheduler {
  constructor(private readonly snipeService: SnipeService) {}

  public start() {
    const stream = interval(1000).pipe(
      mergeMap(() => this.snipeService.getSnipesInFocus()),
      mergeMap(snipe => this.snipeService.placeOffer(snipe))
    )

    return stream.subscribe(() => {

    })
  }
}
