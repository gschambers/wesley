import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { IAnonymousSnipe, IExecutionReport, ISnipe } from '@wesley/common-types'
import { Repository } from 'typeorm'
import { Snipe } from '../entities/Snipe'

@Injectable()
export class SnipeService {
  constructor(
    @InjectRepository(Snipe)
    private readonly snipeRepository: Repository<Snipe>) {}

  public getSnipesByUserId(userId: number): Promise<ISnipe[]> {
    return this.snipeRepository.createQueryBuilder()
      .where('user.id = :userId', { userId })
      .getMany()
  }

  public getSnipesInFocus(): Promise<ISnipe[]> {
    // TODO
  }

  public async createSnipe(snipe: IAnonymousSnipe): Promise<ISnipe> {
    return this.snipeRepository.save(snipe)
  }

  public updateSnipe(snipe: ISnipe): Promise<ISnipe> {
    return this.snipeRepository.save(snipe)
  }

  public deleteSnipe(snipe: ISnipe): Promise<ISnipe> {
    return this.snipeRepository.remove(snipe)
  }

  public placeOffer(snipe: ISnipe): Promise<IExecutionReport> {
    // TODO
  }
}
