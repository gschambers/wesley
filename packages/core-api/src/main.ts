import { NestFactory } from '@nestjs/core'
import { SnipeScheduler } from 'schedulers/snipe.scheduler'
import { AppModule } from 'app.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  const snipeScheduler = app.get(SnipeScheduler)
  snipeScheduler.start()
  await app.listen(process.env.PORT || 3000)
}

bootstrap()
