import { Module } from '@nestjs/common'

import { AdminController } from './controllers/AdminController'
import { ProfileController } from './controllers/ProfileController'
import { SnipeController } from './controllers/SnipeController'
import { SnipeScheduler } from './schedulers/SnipeScheduler'
import { AnalyticsService } from './services/AnalyticsService'
import { SnipeService } from './services/SnipeService'
import { UserService } from './services/UserService'

@Module({
  controllers: [
    AdminController,
    ProfileController,
    SnipeController,
  ],

  providers: [
    AnalyticsService,
    SnipeScheduler,
    SnipeService,
    UserService,
  ],
})
export class AppModule {}
