import { IUser } from '@wesley/common-types'
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class User implements IUser {
  @PrimaryGeneratedColumn()
  id: number
}
