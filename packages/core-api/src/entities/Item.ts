import { IItem } from '@wesley/common-types'
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Item implements IItem {
  @PrimaryGeneratedColumn()
  id: number

  @Column('varchar')
  title: string

  @Column('int8')
  currentBid: number
}
