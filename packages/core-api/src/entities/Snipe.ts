import { ISnipe } from '@wesley/common-types'
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { Item } from './Item'
import { User } from './User'

@Entity()
export class Snipe implements ISnipe {
  @PrimaryGeneratedColumn()
  id: number

  @Column('int8')
  maxBid: number

  @ManyToOne(() => Item)
  item: Item

  @ManyToOne(() => User)
  user: User
}
