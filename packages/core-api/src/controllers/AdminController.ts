import { Controller } from '@nestjs/common'
import { AnalyticsService } from '../services/AnalyticsService'
import { SnipeService } from '../services/SnipeService'
import { UserService } from '../services/UserService'

@Controller('admin')
export class AdminController {
  constructor(
    private readonly analyticsService: AnalyticsService,
    private readonly snipeService: SnipeService,
    private readonly userService: UserService) {}
}
