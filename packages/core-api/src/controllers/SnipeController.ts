import { Controller, Get, Req, UseGuards } from '@nestjs/common'
import { ISnipe } from '@wesley/common-types'
import { Request } from 'express'
import { SessionAuthGuard } from '../guards/SessionAuthGuard'
import { SnipeService } from '../services/SnipeService'

@Controller('snipes')
export class SnipeController {
  constructor(private readonly snipeService: SnipeService) {}

  @UseGuards(SessionAuthGuard)
  @Get()
  public findAllSnipes(@Req() req: Request): Promise<ISnipe[]> {
    const user = req.user
    return this.snipeService.getSnipesByUserId(user.id)
  }
}
