declare module '@wesley/common-types' {
  interface IItem {
    id: number
    title: string
    currentBid: number
  }

  interface IUser {
    id: number
  }

  interface ISnipeGroup {
    id: number
    name: string
  }

  interface ISnipe {
    id: number
    maxBid: number
    item: IItem
    user: IUser
    group?: ISnipeGroup
  }

  type IAnonymousSnipe = Omit<ISnipe, 'id'>

  interface IExecutionReport {

  }
}
